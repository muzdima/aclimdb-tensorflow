import numpy as np
import tensorflow as tf
import pickle
import os
from os import listdir
from os.path import isfile, join

def read_file(path, file_name):
    file = open(join(path, file_name), 'rb')
    result = pickle.load(file)
    file.close()
    return result

def write_file(path, file_name, data):
    file = open(join(path, file_name), 'wb')
    pickle.dump(data, file)
    file.close()

path_word2vec = '.\\word2vec\\'
W = read_file(path_word2vec, 'W')
b = read_file(path_word2vec, 'b')
bb = np.tile(b, (W.shape[0],1))
word2vec = W+b
del W
del b
del bb

path_dict = '.\\dict\\'
word2num = read_file(path_dict, 'word2num')

dict_len = word2vec.shape[0]
embedding = word2vec.shape[1]

def write_features(path_out, file_name, data):
    features = np.array([np.concatenate([word2vec[word2num[((vec[0], vec[1]),)]], np.array(vec[2:])]) for vec in data if ((vec[0], vec[1]),) in word2num])
    write_file(path_out, file_name, features)

def run(path_in, path_out):
    if not os.path.exists(path_out):
        os.makedirs(path_out)
    print('Reading start')
    data = [(file_name, read_file(path_in, file_name)) for file_name in listdir(path_in) if isfile(join(path_in, file_name))]
    print('Reading finish')
    [write_features(path_out, file_name, doc) for (file_name, doc) in data]

run('.\\train\\pre_neg\\', '.\\train\\features_neg\\')
run('.\\train\\pre_pos\\', '.\\train\\features_pos\\')
run('.\\test\\pre_neg\\', '.\\test\\features_neg\\')
run('.\\test\\pre_pos\\', '.\\test\\features_pos\\')
