import nltk
import itertools
import pickle
import os
from os import listdir
from os.path import isfile, join
import collections

def read_file(path, file_name):
    file = open(join(path, file_name), 'rb')
    result = pickle.load(file)
    file.close()
    return result

def write_file(path, file_name, data):
    file = open(join(path, file_name), 'wb')
    pickle.dump(data, file)
    file.close()

path_dict = '.\\dict\\'
word2num = read_file(path_dict, 'word2num')

def run(paths):
    print('Reading start')
    data = [[read_file(path, file_name) for file_name in listdir(path) if isfile(join(path, file_name))] for path in paths]
    print('Reading finish')
    data = [[word2num[((vec[0], vec[1]),)] for vec in doc if ((vec[0], vec[1]),) in word2num] for doc in list(itertools.chain.from_iterable(data))]
    word2vec_data = []
    window = 3
    for doc in data:
        for index, word in enumerate(doc):
            for nb_word in doc[max(index - window, 0) : min(index + window, len(doc)) + 1]: 
                if nb_word != word:
                    word2vec_data.append((word, nb_word))
    word2vec_data = list(set(word2vec_data))
    write_file(path_dict, 'word2vec_data', word2vec_data)

run(['.\\train\\pre_neg\\', '.\\train\\pre_pos\\', '.\\train\\pre_unsup\\'])