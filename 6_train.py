import numpy as np
import tensorflow as tf
import pickle
import os
from os import listdir
from os.path import isfile, join
from datetime import datetime

def read_file(path_file_name):
    file = open(path_file_name, 'rb')
    result = pickle.load(file)
    file.close()
    return result

def run(path_train_neg, path_train_pos, path_test_neg, path_test_pos):
    x_train_neg = np.array([join(path_train_neg, file_name) for file_name in listdir(path_train_neg) if isfile(join(path_train_neg, file_name))])
    y_train_neg = np.zeros(len(x_train_neg))
    x_train_pos = np.array([join(path_train_pos, file_name) for file_name in listdir(path_train_pos) if isfile(join(path_train_pos, file_name))])
    y_train_pos = np.ones(len(x_train_neg))
    x_test_neg = np.array([join(path_test_neg, file_name) for file_name in listdir(path_test_neg) if isfile(join(path_test_neg, file_name))])
    y_test_neg = np.zeros(len(x_train_neg))
    x_test_pos = np.array([join(path_test_pos, file_name) for file_name in listdir(path_test_pos) if isfile(join(path_test_pos, file_name))])
    y_test_pos = np.ones(len(x_train_neg))

    x_train = np.concatenate([x_train_neg, x_train_pos])
    y_train = np.concatenate([y_train_neg, y_train_pos])
    x_test = np.concatenate([x_test_neg, x_test_pos])
    y_test = np.concatenate([y_test_neg, y_test_pos])

    len_train = len(x_train)
    len_test = len(x_test)

    del x_train_neg
    del x_train_pos
    del y_train_neg
    del y_train_pos
    del x_test_neg
    del x_test_pos
    del y_test_neg
    del y_test_pos

    permutation_train = np.random.permutation(len_train)
    x_train = x_train[permutation_train]
    y_train = y_train[permutation_train]
    permutation_test = np.random.permutation(len_test)
    x_test = x_test[permutation_test]
    y_test = y_test[permutation_test]

    del permutation_train
    del permutation_test

    frame_size = 307
    max_time = 1067

    num_hidden = 256

    y_label = tf.placeholder(tf.int64, shape=[None])
    sequence = tf.placeholder(tf.float32, shape=[None, max_time, frame_size])
    sequence_length = tf.placeholder(tf.int32, shape=[None])
    reverse_sequence = tf.reverse_sequence(sequence, sequence_length, batch_axis=0, seq_dim=1)
    with tf.variable_scope('forward_gru'):
        _, state1 = tf.nn.dynamic_rnn(tf.contrib.rnn.GRUCell(num_hidden), sequence, dtype=tf.float32, sequence_length=sequence_length)
    with tf.variable_scope('backward_gru'):
        _, state2 = tf.nn.dynamic_rnn(tf.contrib.rnn.GRUCell(num_hidden), reverse_sequence, dtype=tf.float32, sequence_length=sequence_length)
    out = tf.concat([state1, state2], 1)

    W = tf.get_variable('W', shape=[num_hidden*2, 2], initializer=tf.contrib.layers.xavier_initializer())
    b = tf.get_variable('b', initializer=tf.constant(0.0, shape=[2]))
    
    y = tf.matmul(out, W) + b

    correct_predictions = tf.reduce_sum(tf.cast(tf.equal(tf.argmax(y, 1), y_label), tf.int32))

    loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels=y_label, logits=y))
    train_step = tf.train.AdamOptimizer(1e-4).minimize(loss)

    sess = tf.Session()
    init = tf.global_variables_initializer()
    sess.run(init)

    n_iters = 100
    batch_size = 200
    n_batch = len(x_train)//batch_size
    n_test_batch = len(x_test)//batch_size
    
    for i_iters in range(n_iters):
        for i_batch in range(n_batch):
            data = [read_file(file) for file in x_train[i_batch*batch_size:(i_batch+1)*batch_size]]
            x_batch = np.array([np.pad(doc, [(0,max_time-doc.shape[0]),(0,0)], mode='constant') for doc in data])
            x_batch_len = np.array([doc.shape[0] for doc in data])
            y_batch = y_train[i_batch*batch_size:(i_batch+1)*batch_size]
            sess.run(train_step, {y_label: y_batch, sequence: x_batch, sequence_length: x_batch_len})
            train_loss = sess.run(loss, {y_label: y_batch, sequence: x_batch, sequence_length: x_batch_len})
            print('%s: iters %d/%d; batch %d/%d; current train loss %g' % (str(datetime.now()), i_iters, n_iters, i_batch, n_batch, train_loss))
        correct_predictions_accumulator = 0
        count = 0
        for i_batch in range(n_test_batch):
            data = [read_file(file) for file in x_test[i_batch*batch_size:(i_batch+1)*batch_size]]
            x_batch = np.array([np.pad(doc, [(0,max_time-doc.shape[0]),(0,0)], mode='constant') for doc in data])
            x_batch_len = np.array([doc.shape[0] for doc in data])
            y_batch = y_test[i_batch*batch_size:(i_batch+1)*batch_size]
            correct_predictions_value = sess.run(correct_predictions, {y_label: y_batch, sequence: x_batch, sequence_length: x_batch_len})
            correct_predictions_accumulator += correct_predictions_value
            count += len(data)
        print('%s: iters %d/%d; test acc %g' % (str(datetime.now()), i_iters, n_iters, correct_predictions_accumulator/count))


run('.\\train\\features_neg\\', '.\\train\\features_pos\\', '.\\test\\features_neg\\', '.\\test\\features_pos\\')