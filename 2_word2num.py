import nltk
import itertools
import pickle
import os
from os import listdir
from os.path import isfile, join
import collections

def read_file(path, file_name):
    file = open(join(path, file_name), 'rb')
    result = pickle.load(file)
    file.close()
    return result

def write_file(path, file_name, data):
    file = open(join(path, file_name), 'wb')
    pickle.dump(data, file)
    file.close()

def run(paths):
    print('Reading start')
    data = [[read_file(path, file_name) for file_name in listdir(path) if isfile(join(path, file_name))] for path in paths]
    print('Reading finish')
    
    data = [[(vec[0], vec[1]) for vec in doc] for doc in list(itertools.chain.from_iterable(data))]
    words = [k for (k,v) in collections.Counter(list(itertools.chain.from_iterable([list(nltk.ngrams(words, 1)) for words in data]))).items() if v >= 250]
    
    word2num = {item:index for index, item in enumerate(words)}

    path_dict = '.\\dict\\'
    if not os.path.exists(path_dict):
        os.makedirs(path_dict)
    write_file(path_dict, 'word2num', word2num)

run(['.\\train\\pre_neg\\', '.\\train\\pre_pos\\', '.\\train\\pre_unsup\\'])
