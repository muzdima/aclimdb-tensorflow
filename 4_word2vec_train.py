import numpy as np
import tensorflow as tf
import pickle
import os
from os import listdir
from os.path import isfile, join

def read_file(path, file_name):
    file = open(join(path, file_name), 'rb')
    result = pickle.load(file)
    file.close()
    return result

def write_file(path, file_name, data):
    file = open(join(path, file_name), 'wb')
    pickle.dump(data, file)
    file.close()

def to_one_hot(id):
    hot = np.zeros(dict_len)
    hot[id] = 1
    return hot

path_dict = '.\\dict\\'
word2vec_data = read_file(path_dict, 'word2vec_data')
word2num = read_file(path_dict, 'word2num')
dict_len = len(word2num)
del word2num

embedding = 300
x = tf.placeholder(tf.float32, shape=(None, dict_len))
y_label = tf.placeholder(tf.float32, shape=(None, dict_len))
W = tf.Variable(tf.random_normal([dict_len, embedding]))
b = tf.Variable(tf.random_normal([embedding]))
hidden = tf.add(tf.matmul(x,W), b)
W_ = tf.Variable(tf.random_normal([embedding, dict_len]))
b_ = tf.Variable(tf.random_normal([dict_len]))
y = tf.add(tf.matmul(hidden, W_), b_)
cross_entropy_loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_label, logits=y))

n_iters = 10
batch_size = 10000
n_batch = len(word2vec_data)//batch_size

train_steps = [tf.train.AdamOptimizer(0.05/10**i_iters).minimize(cross_entropy_loss) for i_iters in range(n_iters)]

sess = tf.Session()
init = tf.global_variables_initializer()
sess.run(init)

path_word2vec = '.\\word2vec\\'
if not os.path.exists(path_word2vec):
    os.makedirs(path_word2vec)

for i_iters in range(n_iters):
    batch = word2vec_data[0:batch_size]
    x_train = np.asarray([to_one_hot(x) for (x,y) in batch])
    y_train = np.asarray([to_one_hot(y) for (x,y) in batch])
    for i_batch in range(n_batch):
        sess.run(train_steps[i_iters], {x: x_train, y_label: y_train})
        batch = word2vec_data[i_batch*batch_size:(i_batch+1)*batch_size]
        x_train = np.asarray([to_one_hot(x) for (x,y) in batch])
        y_train = np.asarray([to_one_hot(y) for (x,y) in batch])
        loss = sess.run(cross_entropy_loss, {x: x_train, y_label: y_train})
        print('iters %d/%d; batch %d/%d; loss %g' % (i_iters, n_iters, i_batch, n_batch, loss))
    data_W = sess.run(W)
    data_b = sess.run(b)
    write_file(path_word2vec, 'W', data_W)
    write_file(path_word2vec, 'b', data_b)
    write_file(path_word2vec, 'W'+str(i_iters), data_W)
    write_file(path_word2vec, 'b'+str(i_iters), data_b)